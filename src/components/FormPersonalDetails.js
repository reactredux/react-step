import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

export class FormPersonalDetails extends Component {
    continue = (e) => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = (e) => {
        e.preventDefault();
        this.props.prevStep();
    }

    render() {

        const { values, handleChange } = this.props

        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <AppBar title="Enter Personal Details" />

                    <TextField hintText="Enter your first name" floatingLabelText="First Name" name="firstName" onChange={handleChange} defaultValue={values.firstName} />
                    <br />
                    <TextField hintText="Enter your last name" floatingLabelText="Last Name" name="lastName" onChange={handleChange} defaultValue={values.lastName} />
                    <br />
                    <TextField hintText="Enter your email" floatingLabelText="E-mail" name="email" onChange={handleChange} defaultValue={values.email} />
                    <br />
                    <RaisedButton label='Back' primary={false} style={styles.button} onClick={this.back} />
                    <RaisedButton label='Continue' primary={true} style={styles.button} onClick={this.continue} />
                </React.Fragment>
            </MuiThemeProvider>
        )
    }
}

const styles = {
    button: {
        margin: 15
    }
}

export default FormPersonalDetails
