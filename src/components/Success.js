import React, { Component } from 'react'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from 'material-ui/AppBar'
import RaisedButton from 'material-ui/RaisedButton'

export class Success extends Component {
    backFirst = (e) => {
        e.preventDefault();
        this.props.firstStep();
    }

    render() {

        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <AppBar title="Success" />

                    <h1>Thank you for your Submission</h1>

                    <RaisedButton label='Start Again' primary={true} style={styles.button} onClick={this.backFirst} />
                </React.Fragment>
            </MuiThemeProvider>
        )
    }
}

const styles = {
    button: {
        margin: 15
    }
}

export default Success
