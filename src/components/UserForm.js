import React, { Component } from 'react'

import FormUserDetails from './FormUserDetails'
import FormPersonalDetails from './FormPersonalDetails'
import Confirm from './Confirm'
import Success from './Success'

export class UserForm extends Component {

    state = {
        step: 1,

        firstName: 'anderson',
        lastName: 'botega',
        email: 'anderson.botega@sabium.com',
        occupation: 'programador',
        city: 'maringá',
        bio: 'nenhum'
    }

    firstStep = () => {
        this.setState({ 
            step: 1,
            
            firstName: '',
            lastName: '',
            email: '',
            occupation: '',
            city: '',
            bio: ''
         })
    }

    nextStep = () => {
        this.setState({ step: this.state.step + 1 })
    }

    prevStep = () => {
        this.setState({ step: this.state.step - 1 })
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {

        const { step } = this.state
        const { firstName, lastName, email, occupation, city, bio } = this.state

        const values = { firstName, lastName, email, occupation, city, bio }

        switch (step) {
            case 1:
                return (<FormUserDetails nextStep={this.nextStep} handleChange={this.handleChange} values={values} />)
            case 2:
                return (<FormPersonalDetails nextStep={this.nextStep} prevStep={this.prevStep} handleChange={this.handleChange} values={values} />)
            case 3:
                return (<Confirm nextStep={this.nextStep} prevStep={this.prevStep} values={values} />)
            case 4:
                return (<Success firstStep={this.firstStep} />)
        }
    }
}

export default UserForm
